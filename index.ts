import { ProcessReturnType } from './types';
import { fetchAndParse, findKeyword, findLinks } from './helpers';

async function processPages(
  urls: string[],
  keyword: string,
  depth: number,
  originalUrl?: string
): Promise<ProcessReturnType> {
  if (depth < 1) {
    return { totalCrawled: 0, foundOn: 0, results: [] };
  }

  const fetchedAndParsed = await fetchAndParse(urls);
  const foundMatches = findKeyword(fetchedAndParsed, keyword);
  const deeperResults = await processPages(
    findLinks(fetchedAndParsed, originalUrl ?? urls[0]),
    keyword,
    depth - 1,
    originalUrl ?? urls[0]
  );

  return {
    totalCrawled: fetchedAndParsed.length + deeperResults.totalCrawled,
    foundOn: foundMatches.length + deeperResults.foundOn,
    results: [...foundMatches, ...deeperResults.results],
  };
}

async function main() {
  const parsed = require('yargs/yargs')(process.argv.slice(2))
    .alias('u', 'url')
    .alias('k', 'keyword')
    .alias('d', 'depth')
    .describe('u', 'Website URL you want to crawl')
    .describe('k', 'Keyword you want to find')
    .describe('d', 'How deep you want to search')
    .demandOption(['url', 'keyword']).argv;

  const { url, keyword, depth = 1 } = parsed;
  const { totalCrawled, foundOn, results } = await processPages(
    [url],
    keyword,
    parseInt(depth)
  );

  console.log(
    `Crawled ${totalCrawled} pages. Found ${foundOn} pages with the term ‘${keyword}’:`
  );
  results.forEach(({ url, matches }) =>
    matches.forEach(match => console.log(`${url} => ${match}`))
  );
}

main();
