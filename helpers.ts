import axios from 'axios';
import parseUrl from 'url-parse';
import { htmlToText } from 'html-to-text';
import { ParsedPagesType, ProcessResultType } from './types';

export async function fetchAndParse(
  urls: string[]
): Promise<ParsedPagesType[]> {
  return (await Promise.allSettled(urls.map(async url => await axios.get(url))))
    .filter(p => p.status === 'fulfilled')
    .map(
      page =>
        page.status === 'fulfilled' && {
          url: page.value.config.url,
          data: htmlToText(page.value.data),
        }
    );
}

export function findLinks(pages: ParsedPagesType[], url: string) {
  const originalUrlHost = parseUrl(url).host;
  return pages.reduce((acc, { data }) => {
    return [
      ...acc,
      ...(data
        .match(/\[[^\]]*\]/g)
        ?.map(path => {
          const clearedUrl = path.replace(/[\[\]]/g, '');
          const parsedUrl = parseUrl(clearedUrl);

          if (parsedUrl.host === originalUrlHost) {
            return parsedUrl.href;
          }

          if (!parsedUrl.host && !parsedUrl.href.startsWith('javascript')) {
            return (
              url + (parsedUrl.href.startsWith('/') ? '' : '/') + parsedUrl.href
            );
          }

          return null;
        })
        .filter(Boolean) ?? []),
    ];
  }, []);
}

export function findKeyword(
  pages: ParsedPagesType[],
  keyword: string
): ProcessResultType[] {
  const regExp = new RegExp(`(\\w+.?\\s){0,2}${keyword}(\\s.?\\w+){0,2}`, 'gi');
  return pages
    .map(({ url, data }) => {
      const matches = data.replace(/\[[^\]]*\]/g, '').match(regExp);
      return matches?.length ? { url, matches } : null;
    })
    .filter(Boolean);
}
