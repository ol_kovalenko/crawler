export interface ParsedPagesType {
  url: string;
  data: string;
}

export interface ProcessResultType {
  url: string;
  matches: string[];
}

export interface ProcessReturnType {
  totalCrawled: number;
  foundOn: number;
  results: ProcessResultType[];
}
